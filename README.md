Anime Release Notifier
======================

Fetches your anime "watching" list and notifies you when a new anime episode is available. It also displays the time until a new episode is released.

Supports anime lists from:
- anilist.co
- anime-planet.com
- myanimelist.net
- hummingbird.me

Website:
- https://animereleasenotifier.com/

Forum:
- http://anilist.co/forum/thread/64
- http://myanimelist.net/forum/?topicid=1175519
- https://forums.hummingbird.me/t/16787

Platforms:
- [Android](https://github.com/blitzprog/anime-release-notifier-android)
- [Chrome](https://github.com/blitzprog/anime-release-notifier-chrome)
- [Firefox](https://github.com/blitzprog/anime-release-notifier-firefox)